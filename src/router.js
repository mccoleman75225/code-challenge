const Router = require('koa-router'),
  router = new Router(),
  model = require('./rhinoceros');

  router.get('/rhinoceros/endangered', (ctx, next) => {
    const rhinoceroses = model.findEndangeredRhinos()
    ctx.response.body =  {rhinoceroses} ;
  });

router.post('/rhinoceros', (ctx, next) => {
  ctx.response.body = model.newRhinoceros(ctx.request.body);
});

router.get('/rhinoceros', (ctx, next) => {
  const rhinoceroses = model.getAll(ctx.request.query);
  ctx.response.body = { rhinoceroses };
});

router.get('/rhinoceros/:id', (ctx, next) => {
  const rhino = model.getSingleRhino(ctx.params.id)
  ctx.response.body = { rhino };
});




module.exports = router;
