const uuidv4 = require('uuid/v4');
let rhinoceroses = require('./data');
let rhinocerosesSpecies = ['white_rhinoceros','black_rhinoceros','indian_rhinoceros','javan_rhinoceros','sumatran_rhinoceros']
let listOfKeys = ['name','species'].sort()


exports.getAll = searchField => {
  return rhinoceroses.filter(rhino =>{
    return Object.keys(searchField).every( key => {
      return rhino[key].toLowerCase() === searchField[key].toLowerCase()
    })
  })
};

exports.newRhinoceros = data => {
  let keys = Object.keys(data).sort()
  if(keys.length != listOfKeys.length){
    return {
      status:400,
      message:'Extra keys'
    }
  }
  for(let i = 0; i < keys.length; i++){
    if(keys[i] !== listOfKeys[i]){
      return {
        status:400,
        message:'Keys do not match'
      }
    }
  }

  if(data.name.length >= 20 || data.name.length <= 0 ){
    return {
      status:400,
      message:'Name must be less than 20 characters'
    }
  }
  if(rhinocerosesSpecies.includes(data.species)){
    const newRhino = {
      id: uuidv4(),
      name: data.name,
      species: data.species
    };
    rhinoceroses.push(newRhino);
    return newRhino;
  } else {
    return {
      status:400,
      message:'Rhino species must actually exist'
    }
  }
};

exports.getSingleRhino = rhinoId => {
  return rhinoceroses.find(rhino => {
    return rhino.id == rhinoId
  })
}

exports.findEndangeredRhinos = () => {
   let storageObject = {}
   let arrayOfEndangeredRhinos = []
   for(rhino of rhinoceroses){
     if(storageObject[rhino["species"]]){
       storageObject[rhino["species"]]++
     } else {
       storageObject[rhino["species"]] = 1
     }
   }
   for(species in storageObject){
     if(storageObject[species] < 2){
       arrayOfEndangeredRhinos.push(species)
     }
   }
   return rhinoceroses.filter(rhino => {
     return arrayOfEndangeredRhinos.includes(rhino.species)
   })
};
